#!/usr/bin/env node

const DEFAULT_EXTENSION = "\\.([^.]+)$";
const DEFAULT_REPLACEMENT = ".css.$1";
const DEFAULT_ENCODING = "utf-8";

const fs = require("fs");
const path = require("path");
const glob = require("glob");
const program = require("commander");

const CSSInliner = require("css-inliner");
const CSSInlinerTemplatesJinja2 = require("css-inliner-templates-jinja2");

let input;

let opts = program
    .name("css-inliner-cli")
    .version("1.1.3")

    .arguments("<input>")
    .action((p) => {
        input = p
    })

    .option("-E, --extension <regex>", "extension regex", DEFAULT_EXTENSION)
    .option("-R, --replacement <string>", "extension replacement", DEFAULT_REPLACEMENT)
    .option("-T, --template <handlebars|jinja2>", "templating language to use")
    .option("-e, --encoding <format>", "encoding of the input file", DEFAULT_ENCODING)
    .option("-d, --debug", "toggle debugging information")

    .parse(process.argv)
    .opts();

if (typeof input == "undefined") {
    console.error("no input specified");
    process.exit(1);
}

if (opts.debug) {
    console.debug(`css inlining for ${input}`);

    for (let key in opts) {
        console.debug(`  ${key}: ${opts[key]}`);
    }
}

/**
 * Return the template function associated with the specified option value. If
 * no such function exists, the return value is undefined.
 *
 * @param {string} opt
 * @returns {function}
 */
function getTemplate(opt) {
    switch (opt) {
    case "handlebars":
        return CSSInliner.handlebars;
    case "jinja2":
        return CSSInlinerTemplatesJinja2;
    }
}

let template = getTemplate(opts.template);

// replace string pattern with actual regex
opts.extension = new RegExp(opts.extension);

glob.glob(input, (error, matches) => {
    if (error) {
        console.error(error);
        process.exit(1);
    }

    if (!matches.length) {
        console.log("no matching files");
        process.exit();
    }

    let stats, html, inliner;

    matches.forEach((match) => {
        if (opts.debug) {
            console.debug(`matched ${match}`);
        }

        stats = fs.lstatSync(match);

        if (!stats.isFile()) {
            console.error(`matched directory ${match}, skipping`);
            return;
        }

        html = fs.readFileSync(match, opts.encoding);

        let directory = path.dirname(match);

        inliner = new CSSInliner({
            directory: directory,
            template: template,
            loadAsync: (file) => {
                return new Promise(((resolve, reject) => {
                    let stylesheet;

                    if (file.startsWith("/")) {
                        stylesheet = file;
                    } else {
                        stylesheet = path.join(directory, file);
                    }

                    if (opts.debug) {
                        console.debug(`loading ${stylesheet}`);
                    }

                    if (!fs.existsSync(stylesheet)) {
                        console.error("stylesheet does not exist");
                        reject();
                    }

                    resolve(fs.readFileSync(stylesheet, opts.encoding));
                }));
            }
        });

        inliner.inlineCSSAsync(html).then(
            (result) => {
                let file = match.replace(opts.extension, opts.replacement);

                fs.writeFileSync(file, result);

                if (opts.debug) {
                    console.debug(`written to ${file}`);
                }
            }, console.error
        );
    })
});
