module.exports = {
    root: true,
    env: {
        node: true,
        es6: true
    },
    extends: "eslint:recommended",
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly"
    },
    parser: "babel-eslint",
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: "module"
    },
    rules: {
        "indent": [
            "error", 4
        ],
        "quotes": [
            "error", "double"
        ],
        "array-bracket-spacing": [
            "error", "always"
        ],
        "no-trailing-spaces": [
            "error",
            {
                skipBlankLines: true,
                ignoreComments: true
            }
        ]
    }
}
