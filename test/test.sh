#!/bin/bash

node index.js -d               "test/**/*[!.css].html"
node index.js -d -T jinja2     "test/**/*[!.css].jinja2"
node index.js -d -T handlebars "test/**/*[!.css].handlebars"
